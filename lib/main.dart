import 'package:flutter/material.dart';
import 'package:dynamic_color/dynamic_color.dart';

import 'share_bite_theme.dart';

import 'home.dart';

void main() => runApp(const ShareBite());

class ShareBite extends StatelessWidget {
  const ShareBite({super.key});

  @override
  Widget build(BuildContext context) {
    return DynamicColorBuilder(
      builder: ((lightDynamic, darkDynamic) {
        return MaterialApp(
          title: 'Share a Bite',
          theme: ThemeData(
            useMaterial3: true,
            colorScheme: lightDynamic ?? ShareBiteTheme.lightColorScheme,
            textTheme: ShareBiteTheme.lightTextTheme,
          ),
          darkTheme: ThemeData(
            useMaterial3: true,
            textTheme: ShareBiteTheme.darkTextTheme,
            colorScheme: darkDynamic ?? ShareBiteTheme.darkColorScheme,
          ),
          home: const Home(),
        );
      }),
    );
  }
}
