import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class ShareBiteTheme {
  static const ColorScheme lightColorScheme = ColorScheme(
    brightness: Brightness.light,
    primary: Color(0xFFC00016),
    onPrimary: Color(0xFFFFFFFF),
    primaryContainer: Color(0xFFFFDAD6),
    onPrimaryContainer: Color(0xFF410003),
    secondary: Color(0xFF775653),
    onSecondary: Color(0xFFFFFFFF),
    secondaryContainer: Color(0xFFFFDAD6),
    onSecondaryContainer: Color(0xFF2C1513),
    tertiary: Color(0xFF725B2E),
    onTertiary: Color(0xFFFFFFFF),
    tertiaryContainer: Color(0xFFFEDFA6),
    onTertiaryContainer: Color(0xFF261900),
    error: Color(0xFFBA1A1A),
    errorContainer: Color(0xFFFFDAD6),
    onError: Color(0xFFFFFFFF),
    onErrorContainer: Color(0xFF410002),
    background: Color(0xFFFFFBFF),
    onBackground: Color(0xFF201A19),
    surface: Color(0xFFFFFBFF),
    onSurface: Color(0xFF201A19),
    surfaceVariant: Color(0xFFF5DDDB),
    onSurfaceVariant: Color(0xFF534341),
    outline: Color(0xFF857371),
    onInverseSurface: Color(0xFFFBEEEC),
    inverseSurface: Color(0xFF362F2E),
    inversePrimary: Color(0xFFFFB4AC),
    shadow: Color(0xFF000000),
    surfaceTint: Color(0xFFC00016),
  );

  static const ColorScheme darkColorScheme = ColorScheme(
    brightness: Brightness.dark,
    primary: Color(0xFFFFB4AC),
    onPrimary: Color(0xFF690007),
    primaryContainer: Color(0xFF93000E),
    onPrimaryContainer: Color(0xFFFFDAD6),
    secondary: Color(0xFFE7BDB8),
    onSecondary: Color(0xFF442927),
    secondaryContainer: Color(0xFF5D3F3C),
    onSecondaryContainer: Color(0xFFFFDAD6),
    tertiary: Color(0xFFE0C38C),
    onTertiary: Color(0xFF3F2D04),
    tertiaryContainer: Color(0xFF584419),
    onTertiaryContainer: Color(0xFFFEDFA6),
    error: Color(0xFFFFB4AB),
    errorContainer: Color(0xFF93000A),
    onError: Color(0xFF690005),
    onErrorContainer: Color(0xFFFFDAD6),
    background: Color(0xFF201A19),
    onBackground: Color(0xFFEDE0DE),
    surface: Color(0xFF201A19),
    onSurface: Color(0xFFEDE0DE),
    surfaceVariant: Color(0xFF534341),
    onSurfaceVariant: Color(0xFFD8C2BF),
    outline: Color(0xFFA08C8A),
    onInverseSurface: Color(0xFF201A19),
    inverseSurface: Color(0xFFEDE0DE),
    inversePrimary: Color(0xFFC00016),
    shadow: Color(0xFF000000),
    surfaceTint: Color(0xFFFFB4AC),
  );

  static final TextTheme lightTextTheme = TextTheme(
    bodyText1: GoogleFonts.rubik(
      fontSize: 14.0,
      fontWeight: FontWeight.w700,
      color: Colors.black,
    ),
    headline1: GoogleFonts.rubik(
      fontSize: 32.0,
      fontWeight: FontWeight.bold,
      color: Colors.black,
    ),
    headline2: GoogleFonts.rubik(
      fontSize: 21.0,
      fontWeight: FontWeight.w700,
      color: Colors.black,
    ),
    headline3: GoogleFonts.rubik(
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
      color: Colors.black,
    ),
    headline6: GoogleFonts.rubik(
      fontSize: 20.0,
      fontWeight: FontWeight.w600,
      color: Colors.black,
    ),
  );

  static final TextTheme darkTextTheme = TextTheme(
    bodyText1: GoogleFonts.rubik(
      fontSize: 14.0,
      fontWeight: FontWeight.w700,
      color: Colors.white,
    ),
    headline1: GoogleFonts.rubik(
      fontSize: 32.0,
      fontWeight: FontWeight.bold,
      color: Colors.white,
    ),
    headline2: GoogleFonts.rubik(
      fontSize: 21.0,
      fontWeight: FontWeight.w700,
      color: Colors.white,
    ),
    headline3: GoogleFonts.rubik(
      fontSize: 16.0,
      fontWeight: FontWeight.w600,
      color: Colors.white,
    ),
    headline6: GoogleFonts.openSans(
      fontSize: 20.0,
      fontWeight: FontWeight.w600,
      color: Colors.white,
    ),
  );

  TextTheme checkCurrentTheme() {
    if (ThemeMode.system == ThemeMode.dark) {
      return darkTextTheme;
    }

    return lightTextTheme;
  }
}
